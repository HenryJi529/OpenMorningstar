class Suit:
    SPADES = 'Spades'
    HEARTS = 'Hearts'
    CLUBS = 'Clubs'
    DIAMONDS = 'Diamonds'


class Rank:
    ACE = 'A'
    TWO = '2'
    THREE = '3'
    FOUR = '4'
    FIVE = '5'
    SIX = '6'
    SEVEN = '7'
    EIGHT = '8'
    NINE = '9'
    TEN = '10'
    JACK = 'J'
    QUEEN = 'Q'
    KING = 'K'


class Country:
    WEI = '魏'
    SHU = '蜀'
    WU = '吴'
    QUN = '群'


class Sex:
    MALE = True
    FEMALE = False


class Character:
    Lord = "主公"
    Rebel = "反贼"
    Loyal = "忠臣"
    Traitor = "内奸"